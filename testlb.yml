---
- name: prepare systems for load-balancer testing
  hosts:
  - all
  become: true
  gather_facts: no
  tasks:
  - name: Create directories
    file:
      state: directory
      recurse: true
      path: /opt/testlb/certs

  - name: Create keypair
    command: openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj "/C=nl/ST=OV/L=Zwolle/O=BAT/CN=testing.cert/" -keyout /opt/testlb/certs/testlb.key -out /opt/testlb/certs/testlb.crt
    args:
      creates: /opt/testlb/certs/testlb.crt

  - name: Create daemon code
    copy:
      dest: /opt/testlb/testlb.py
      mode: 0755
      content: | 
        #!/usr/bin/env python
        from threading import Thread
        from SocketServer import ThreadingMixIn
        from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
        import socket
        import ssl

        HOSTNAME = socket.gethostname()

        class Router_Status_Handler(BaseHTTPRequestHandler):
          def do_GET(self):
            if self.path == '/healthz':
              response=200
              body='ok'
            elif self.path == '/':
              response=200
              body='Hello from router status %s' % (HOSTNAME)
            else:
              response=404
              body=''
            self.send_response(response)
            self.send_header('Content-Type', 'text/plain')
            self.end_headers()
            self.wfile.write(body)

        class Router_Handler(BaseHTTPRequestHandler):
          def do_GET(self):
            if self.path == '/':
              response=200
              body='Hello from apps %s' % (HOSTNAME)
            else:
              response=404
              body=''
            self.send_response(response)
            self.send_header('Content-Type', 'text/plain')
            self.end_headers()
            self.wfile.write(body)

        class API_Handler(BaseHTTPRequestHandler):
          def do_GET(self):
            if self.path == '/readyz':
              response=200
              body='ok'
            elif self.path == '/':
              response=200
              body='Hello from api %s' % (HOSTNAME)
            else:
              response=404
              body=''
            self.send_response(response)
            self.send_header('Content-Type', 'text/plain')
            self.end_headers()
            self.wfile.write(body)

        class Machine_Handler(BaseHTTPRequestHandler):
          def do_GET(self):
            if self.path == '/healthz':
              response=200
              body=''
            elif self.path == '/':
              response=200
              body='Hello from machine config %s' % (HOSTNAME)
            else:
              response=404
              body=''
            self.send_response(response)
            self.send_header('Content-Type', 'text/plain')
            self.end_headers()
            self.wfile.write(body)

        class ThreadingHTTPServer(ThreadingMixIn, HTTPServer):
          daemon_threads = True


        def serve_on_port(port, handler, SSL=True):
          server = ThreadingHTTPServer(('0.0.0.0', port), handler)
          if SSL:
            server.socket = ssl.wrap_socket(server.socket,
                                            keyfile='/opt/testlb/certs/testlb.key',
                                            certfile='/opt/testlb/certs/testlb.crt',
                                            server_side=True)
          server.serve_forever()

        if __name__ == '__main__':
          Thread(target=serve_on_port, args=[1936, Router_Status_Handler, True]).start()
          Thread(target=serve_on_port, args=[80, Router_Handler, False]).start()
          Thread(target=serve_on_port, args=[443, Router_Handler, True]).start()
          Thread(target=serve_on_port, args=[6443, API_Handler, True]).start()
          serve_on_port(22623, Machine_Handler)

  - name: Create systemd unit file
    copy:
      dest: /etc/systemd/system/testlb.service
      mode: 0644
      content: |
        [Unit]
        Description=Test Load Balancer configs
        After=network.target

        [Service]
        Type=simple
        User=root
        ExecStart=/opt/testlb/testlb.py
        Restart=always

        [Install]
        WantedBy=multi-user.target

  - name: open firewall ports
    firewalld:
      port: "{{ item.port | default(omit) }}"
      service: "{{ item.service | default(omit) }}"
      permanent: yes
      immediate: yes
      state: enabled
    loop:
    - port: 1936/tcp
    - port: 6443/tcp
    - port: 22623/tcp
    - service: http
    - service: https

  - name: stop and disable httpd
    systemd:
      name: httpd
      state: stopped
      enabled: false
    failed_when: false

  - name: Start and enable testlb service
    systemd:
      name: testlb
      daemon_reload: yes
      state: started
      enabled: yes
