#!/usr/bin/env python
from threading import Thread
from SocketServer import ThreadingMixIn
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
import socket
import ssl

HOSTNAME = socket.gethostname()

class Router_Status_Handler(BaseHTTPRequestHandler):
  def do_GET(self):
    if self.path == '/healthz':
      response=200
      body='ok'
    elif self.path == '/':
      response=200
      body='Hello from router status %s' % (HOSTNAME)
    else:
      response=404
      body=''
    self.send_response(response)
    self.send_header('Content-Type', 'text/plain')
    self.end_headers()
    self.wfile.write(body)

class Router_Handler(BaseHTTPRequestHandler):
  def do_GET(self):
    if self.path == '/':
      response=200
      body='Hello from apps %s' % (HOSTNAME)
    else:
      response=404
      body=''
    self.send_response(response)
    self.send_header('Content-Type', 'text/plain')
    self.end_headers()
    self.wfile.write(body)

class API_Handler(BaseHTTPRequestHandler):
  def do_GET(self):
    if self.path == '/readyz':
      response=200
      body='ok'
    elif self.path == '/':
      response=200
      body='Hello from api %s' % (HOSTNAME)
    else:
      response=404
      body=''
    self.send_response(response)
    self.send_header('Content-Type', 'text/plain')
    self.end_headers()
    self.wfile.write(body)

class Machine_Handler(BaseHTTPRequestHandler):
  def do_GET(self):
    if self.path == '/healthz':
      response=200
      body=''
    elif self.path == '/':
      response=200
      body='Hello from machine config %s' % (HOSTNAME)
    else:
      response=404
      body=''
    self.send_response(response)
    self.send_header('Content-Type', 'text/plain')
    self.end_headers()
    self.wfile.write(body)

class ThreadingHTTPServer(ThreadingMixIn, HTTPServer):
  daemon_threads = True


def serve_on_port(port, handler, SSL=True):
  server = ThreadingHTTPServer(('0.0.0.0', port), handler)
  if SSL:
    server.socket = ssl.wrap_socket(server.socket,
                                    keyfile='/opt/testlb/certs/testlb.key',
                                    certfile='/opt/testlb/certs/testlb.crt',
                                    server_side=True)
  server.serve_forever()

if __name__ == '__main__':
  Thread(target=serve_on_port, args=[1936, Router_Status_Handler, True]).start()
  Thread(target=serve_on_port, args=[80, Router_Handler, False]).start()
  Thread(target=serve_on_port, args=[443, Router_Handler, True]).start()
  Thread(target=serve_on_port, args=[6443, API_Handler, True]).start()
  serve_on_port(22623, Machine_Handler)

