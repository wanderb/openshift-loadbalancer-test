# openshift-loadbalancer-test

This is a simple playbook to deploy a service (`testlb.service`) on all nodes
in a specified inventory.

This service will listen on ports TCP/80, TCP/444, TCP/1936, TCP/6443, and
TCP/22623 to emulate the various OpenShift API and ROuter services, so a
Load-Balancer configuration can be tested before a cluster is installed.

## Usage
`ansible-playbook -i hosts testlb.yml`

